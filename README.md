<!-- README.md is generated from README.Rmd. Please edit that file -->

scatter
=======

<!-- badges: start -->

[![pipeline
status](https://gitlab.com/nehemie/scatter/badges/main/pipeline.svg)](https://gitlab.com/nehemie/scatter/-/commits/main)
<!-- badges: end -->

The goal of scatter is to examine growing centralised state’s power.

Installation
------------

You can install the development version of scatter like so:

    library(devtools)
    install_gitlab("nehemie/scatter", host = "https://gitlab.com")

Example
-------

This is a basic example which shows you how to solve a common problem:

    library(scatter)
    require(sf)
    #> Loading required package: sf
    #> Linking to GEOS 3.10.1, GDAL 3.3.3, PROJ 8.2.0
    require(ggplot2)
    #> Loading required package: ggplot2
    data(katzar)
    dkatzar <- katzar
    lut <- periodise() 
    tstep <- define_step(100, lut)
    st_geometry(dkatzar) <- NULL
    cs <- weight_aoristicly(data=dkatzar, lookup=lut, step_width=tstep)
    katzar$aoristic_sum <- apply(cs,1,sum)
    ggplot() + geom_sf(data = katzar,
                       aes(size=aoristic_sum, colour=province), alpha=0.3) +
      theme_minimal() +
      theme(plot.caption = element_text(hjust = 1, colour="sienna"),
            legend.position = "bottom") +
      ggtitle("Aoristic Total Weight of Sites",
              subtitle ="Coloured by province") +
      labs(caption = "Made with scatter", color="Province")

<img src="man/figures/README-example-1.png" width="80%" />
