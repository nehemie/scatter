---
title: "Aorist Analysis of KatzaS"
autor: "Néhémie Strupler"
date: "Cambridge, December 2021"
output: rmarkdown::html_vignette
vignette: >
  %\VignetteIndexEntry{Aorist Analysis of KatzaS}
  %\VignetteEngine{knitr::rmarkdown}
  %\VignetteEncoding{UTF-8}
  %\VignetteSuggest{ggplot2}
  %\VignetteSuggest{cowplot}
---

```{r, include = FALSE}
knitr::opts_chunk$set(
  collapse = TRUE,
  comment = "#>",
  out.width = "99%",
  fig.width= 8,
  dpi = 300,
fig.retina = 1

)
```

```{r, load library}
library(scatter)
require(magrittr)
require(ggplot2)
require(cowplot)
```



```{r load_data}
data(katzar)
dkatzar <- katzar
dkatzar <- dkatzar[,]
sf::st_geometry(dkatzar) <- NULL
dkatzar  %>% names
col_of_periods <- c("eba", "mba", "lba", "eia", "ia", "lia", "he")

  ## Remove "?" from datation
dkatzar[, col_of_periods] <- apply(dkatzar[, col_of_periods], 2,
                                  function(x) gsub("\\?", "", x,
                                                   perl = TRUE) )
```

## Period table

```{r periodise}

lookup_periods <- periodise()
```

```{r periodise_kable, echo=FALSE}

knitr::kable(lookup_periods)

```



## Step width


Define the time setps length for the aoristic analysis

```{r stepw}

step_width <- define_step(step_width = 100,
                          lookup_periods = lookup_periods)

```

## Distribute periods

Generate the sequence of dates based on the periods dates and the time steps

```{r periods_cadanced}

periods_cadanced <-
   split_periods(lookup_periods =  lookup_periods,
                 step_width = 50)

```

```{r periodise_candanced_kable, echo=FALSE}

periods_cadanced

```


## Create a matrix to insert the aoristic values


```{r matrixes}

aoristicised_matrix <-
   weight_aoristicly(dataset = dkatzar,
                   lookup_periods = lookup_periods,
                   step_width = 100)

sites_count_matrix <-
   count_sites(dataset = dkatzar,
                   lookup_periods = lookup_periods,
                   step_width = 100)

```

```{r matrixes kable, echo = FALSE}

knitr::kable(aoristicised_matrix[1:3,1:3])

knitr::kable(sites_count_matrix[1:3,1:3])

```

## Sum of aoristic values

```{r}

sum_sites_aoristed <- sum_time_steps(aoristicised_matrix)

sum_sites_count <- sum_time_steps(sites_count_matrix)
```


## Visualisation Aoristic

```{r}

ggplot(sum_sites_aoristed,
       aes(x = date, y = sum)) +
  geom_line(col=3) +
  scale_x_continuous(breaks = lookup_periods$EndDate) +
  ylab("Aggregated aoristic weight") +
  xlab("Year (B.)C.E.") +
#   theme_light() +
  labs(title ="Sum of Aoristic Weights",
       caption = "Data: katzar/KatzaS") -> g_sites_aoristed
 g_sites_aoristed

```

```{r}
ggplot(sum_sites_count,
       aes(x=date, y=sum)) +
  geom_line(col=4) +
  scale_x_continuous( breaks = lookup_periods$StartDate) +
  ylab("Number of sites") +
  xlab("Year (B.)C.E.") +
#   theme_light() +
  labs(title ="Temporal Density of Sites",
       caption = "Data: katzar/KatzaS") -> g_sites_counted
g_sites_counted
```

# Visualisation Aoristic and Count

```{r, warning = FALSE, message = FALSE, echo=FALSE}
# Vertical dual plot - saved
 plimits <- c(-1900,-400)
 swqplimits <-  seq(plimits[1],plimits[2], by=step_width*2)
plot_grid(g_sites_counted +
          scale_x_continuous(breaks = swqplimits, limit=plimits),
          g_sites_aoristed +
          scale_x_continuous(breaks = swqplimits, limit=plimits),
          align = "v",
          ncol = 1)
#ggsave("count_vs_ao.pdf")
```
