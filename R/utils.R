# R             | 2021-12-10 |
# ===== utils.R ==============

#' test if ‘x’ contains integer
#' @param x an integer
#' @description ‘is.integer(x)’ does *not* test if ‘x’ contains
#' integer numbers. Fonction copied from is.integer()
#' @return logical
#' @seealso [base::is.integer()]
#' @keywords internal
#' @noRd
#' @examples
#' is.wholenumber(1) # is TRUE
is.wholenumber <- function(x, tol = .Machine$double.eps^0.5){
    abs(x - round(x)) < tol
  }


# bye-bye....................|
